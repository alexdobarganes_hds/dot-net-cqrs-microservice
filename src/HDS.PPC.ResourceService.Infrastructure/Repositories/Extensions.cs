﻿using Convey;
using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Application.Services;
using HDS.PPC.ResourceService.Domain.Aggregates;
using HDS.PPC.ResourceService.Infrastructure.Postgres.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace HDS.PPC.ResourceService.Infrastructure.Repositories
{
    internal static class Extensions
    {
        internal static IConveyBuilder AddDbRepositories<TAggregate, TDto, TMapper>(this IConveyBuilder builder) where TAggregate : AggregateRoot
        {
            builder.Services.AddScoped<IRepository<TAggregate>, PostgresRepository<TAggregate>>();
            builder.Services.AddScoped(typeof(IAggregateMapper<TAggregate, TDto>), typeof(TMapper));

            return builder;
        }
    }
}
