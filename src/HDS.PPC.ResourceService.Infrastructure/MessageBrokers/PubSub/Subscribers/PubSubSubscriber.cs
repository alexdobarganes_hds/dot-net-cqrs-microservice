﻿using Convey.MessageBrokers;
using Google.Cloud.PubSub.V1;
using HDS.PPC.ResourceService.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Infrastructure.MessageBrokers.PubSub
{
    public class PubSubSubscriber : IBusSubscriber
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IApplicationBuilder _app;
        private readonly PubSubConfig _pubSubConfig;
        private readonly ILogger<PubSubSubscriber> _logger;
        private readonly IExceptionToMessageMapper _exceptionToMessageMapper;
        private readonly IBusPublisher _busPublisher;
        private bool _disposedValue;

        public PubSubSubscriber(IApplicationBuilder app)
        {
            _app = app;
            _serviceProvider = app.ApplicationServices;
            _pubSubConfig = _serviceProvider.GetRequiredService<PubSubConfig>();
            _logger = _serviceProvider.GetRequiredService<ILogger<PubSubSubscriber>>();
            _exceptionToMessageMapper = _serviceProvider.GetService<IExceptionToMessageMapper>() ?? new EmptyExceptionToMessageMapper();
            _busPublisher = _serviceProvider.GetRequiredService<IBusPublisher>();
        }


        private Task TryHandle<TEvent>(TEvent @event, Func<IServiceProvider, TEvent, object, Task> handle)
        {
            return handle(_serviceProvider, @event, null);
        }

        private async Task<PubsubMessage> GetMessage(HttpRequest request)
        {
            request.EnableBuffering();
            dynamic bodyContent;
            using (var reader = new StreamReader(request.Body, System.Text.Encoding.UTF8, false, leaveOpen: true))
            {
                bodyContent = await reader.ReadToEndAsync();
                request.Body.Position = 0;
            }

            var message = JObject.Parse(bodyContent).message;

            return PubsubMessage.Parser.ParseJson(JsonConvert.SerializeObject(message));
        }


        public IBusSubscriber Subscribe<T>(Func<IServiceProvider, T, object, Task> handle) where T : class
        {
            _logger.Log(LogLevel.Information, "PubSubSubscriber successfully registered {@eventType}", typeof(T).FullName);

            _app.Use(async (context, next) =>
            {
                var shouldBeHandled = context.Request.Path.Value.Contains(_pubSubConfig.Endpoint);
                if (!shouldBeHandled)
                    await next();
                else
                {
                    var request = context.Request;
                    var pubSubMessage = await GetMessage(request);
                    var messageTypeName = typeof(T).Name;

                    if (pubSubMessage.Attributes["eventType"] != messageTypeName)
                        await next();
                    else
                    {
                        var payload = pubSubMessage.Data.ToStringUtf8();
                        var message = JsonConvert.DeserializeObject<T>(payload);
                        var messageName = message.GetMessageName();

                        _logger.LogInformation("HTTP Request from PubSub for type {@messageTypeName} with content {@payload}", messageTypeName, payload);

                        try
                        {
                            _logger.LogInformation("Processing {@messageName} for type {@messageTypeName} with content {@payload}", messageName, messageTypeName, message);
                            await TryHandle(message, handle);
                        }
                        catch (Exception ex)
                        {
                            var rejectedEvent = _exceptionToMessageMapper.Map(ex, message);
                            if (rejectedEvent is null)
                            {
                                throw new Exception($"Unable to handle a message: '{messageTypeName}' ", ex);
                            }

                            await _busPublisher.PublishAsync(rejectedEvent);
                            _logger.LogWarning("Published a rejected event: {@rejectedEvent} for the message: {@messageName}.", rejectedEvent.GetMessageName(), messageName);
                        }

                        _logger.LogInformation("Message {@messageName} handled.", messageName);

                        context.Response.StatusCode = StatusCodes.Status200OK;
                        await Task.CompletedTask;
                    }
                }
            });
            return this;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        private class EmptyExceptionToMessageMapper : IExceptionToMessageMapper
        {
            public object Map(Exception exception, object message) => null;
        }
    }
}