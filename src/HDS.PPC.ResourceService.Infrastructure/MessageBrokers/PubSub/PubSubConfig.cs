﻿namespace HDS.PPC.ResourceService.Infrastructure.MessageBrokers.PubSub
{
    public class PubSubConfig
    {
        public string GcpProjectId { get; set; }
        public object EventTypePrefix { get; set; }
        public string TopicId { get; set; }
        public string Endpoint { get; set; }
        public string LifeCycle { get; set; }
        public PubSubEmulator Emulator { get; set; }

        public PubSubConfig()
        {
            Endpoint = "pubsubEvents";
        }
    }

    public class PubSubEmulator
    {
        public string EmulatorHostAndPort { get; set; }
    }
}