using Convey.MessageBrokers;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;
using HDS.PPC.ResourceService.Application.Events;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Infrastructure.MessageBrokers.PubSub
{
    public class PubSubPublisher : IBusPublisher
    {
        private readonly ILogger<PubSubPublisher> _logger;
        private readonly PubSubConfig _publisherConfig;

        public PubSubPublisher(ILogger<PubSubPublisher> logger, PubSubConfig publisherConfig)
        {
            _logger = logger;
            _publisherConfig = publisherConfig;
        }

        private async Task<string> PublishMessage(TopicName topicName, PubsubMessage pubsubMessage)
        {
            var client = await PublisherClient.CreateAsync(topicName).ConfigureAwait(false);
            return await client.PublishAsync(pubsubMessage).ConfigureAwait(false);
        }

        private async Task<string> PublishMessageThruEmulator(TopicName topicName, PubsubMessage pubsubMessage)
        {
            PublisherServiceApiClient client = new PublisherServiceApiClientBuilder
            {
                Endpoint = _publisherConfig.Emulator.EmulatorHostAndPort,
                ChannelCredentials = ChannelCredentials.Insecure
            }.Build();

            var response = await client.PublishAsync(topicName, new[] { pubsubMessage }).ConfigureAwait(false);
            return response.MessageIds.Single();
        }

        public async Task PublishAsync<T>(T message, string messageId = null, string correlationId = null,
            string spanContext = null, object messageContext = null, IDictionary<string, object> headers = null)
            where T : class
        {
            var topicName = TopicName.FromProjectTopic(_publisherConfig.GcpProjectId, $"{_publisherConfig.LifeCycle}-{GetTopicFromAttribute(message) ?? _publisherConfig.TopicId}");

            try
            {
                var pubsubMessage = BuildPubSubMessage(message, headers);

                if (_publisherConfig.Emulator != null)
                    messageId = await PublishMessageThruEmulator(topicName, pubsubMessage).ConfigureAwait(false);
                else
                    messageId = await PublishMessage(topicName, pubsubMessage).ConfigureAwait(false);

                _logger.LogInformation("Published message with {@messageId} to {@topic}", messageId, topicName.ToString());
            }
            catch (Exception exception)
            {
                _logger.LogInformation("An error ocurred when publishing message {@message}: {@exceptionMessage} to {@topic}",
                    message, exception.Message, topicName.ToString());
                throw;
            }
        }

        private PubsubMessage BuildPubSubMessage<T>(T message, IDictionary<string, object> headers)
        {
            var eventType = $"{GetLocalEventTypeFromAttribute(message)}";
            var pubsubInfo = GetPublishInfoFromAttribute(message);

            if (pubsubInfo != null)
                eventType = $"{pubsubInfo.Prefix}.{GetEventTypeFromAttribute(message)}";

            var pubsubMessage = new PubsubMessage
            {
                // The data is any arbitrary ByteString. Here, we're using text.
                Data = ByteString.CopyFromUtf8(JsonConvert.SerializeObject(message))
            };

            pubsubMessage.Attributes.Add("eventType", eventType);
            //pubsubMessage.Attributes.Add("subject", subject);

            //Additional Headers
            if (headers != null)
            {
                foreach (var key in headers.Keys)
                {
                    pubsubMessage.Attributes.Add(key, headers[key].ToString());
                }
            }

            return pubsubMessage;
        }

        private static string GetEventTypeFromAttribute<T>(T message) => message.GetType().Name;
        private static string GetLocalEventTypeFromAttribute<T>(T message) => message.GetType().Name;
        private static string GetTopicFromAttribute<T>(T message) => message.GetType().GetCustomAttribute<PubSubInfoAttribute>()?.TopicId;
        private static PubSubInfoAttribute GetPublishInfoFromAttribute<T>(T message) => message.GetType().GetCustomAttribute<PubSubInfoAttribute>();
    }
}