using Convey;
using Convey.MessageBrokers;
using Convey.MessageBrokers.CQRS;
using HDS.PPC.ResourceService.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("HDS.PPC.ResourceService.Integration.Tests")]
namespace HDS.PPC.ResourceService.Infrastructure.MessageBrokers.PubSub
{
    internal static class Extensions
    {
        internal static IConveyBuilder AddExceptionToMessageMapper<T>(this IConveyBuilder builder)
            where T : class, IExceptionToMessageMapper
        {
            builder.Services.AddSingleton<IExceptionToMessageMapper, T>();
            return builder;
        }

        internal static string GetMessageName(this object message)
            => message.GetType().Name.Underscore().ToLowerInvariant();

        private const string SectionName = "pubsub";

        internal static IConveyBuilder AddPubSub(this IConveyBuilder builder, string sectionName = SectionName)
        {
            if (string.IsNullOrWhiteSpace(sectionName))
            {
                sectionName = SectionName;
            }

            var options = builder.GetOptions<PubSubConfig>(sectionName);

            builder.AddServiceBusCommandDispatcher()
                   .AddServiceBusEventDispatcher();

            builder.Services.AddSingleton(options);
            builder.Services.AddSingleton<IBusPublisher, PubSubPublisher>();
            builder.Services.AddSingleton<IBusSubscriber, PubSubSubscriber>();
            builder.Services.AddTransient<ICorrelationContextAccessor, CorrelationContextAccessor>();

            return builder;
        }

        internal static IApplicationBuilder UsePubSub(this IApplicationBuilder app, Func<IBusSubscriber, IBusSubscriber> sub)
        {
            sub.Invoke(new PubSubSubscriber(app));
            return app;
        }
    }
}