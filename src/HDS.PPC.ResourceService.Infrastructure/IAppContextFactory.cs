using HDS.PPC.ResourceService.Application;

namespace HDS.PPC.ResourceService.Infrastructure
{
    public interface IAppContextFactory
    {
        IAppContext Create();
    }
}