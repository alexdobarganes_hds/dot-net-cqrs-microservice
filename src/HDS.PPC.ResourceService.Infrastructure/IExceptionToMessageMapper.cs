﻿using System;

namespace HDS.PPC.ResourceService.Infrastructure.Exceptions
{
    public interface IExceptionToMessageMapper
    {
        object Map(Exception exception, object message);
    }
}