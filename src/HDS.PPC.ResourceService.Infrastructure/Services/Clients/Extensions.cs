﻿using Convey;
using Convey.HTTP;
using Microsoft.Extensions.DependencyInjection;

namespace HDS.PPC.ResourceService.Infrastructure.Services
{
    internal static class Extensions
    {
        internal static IConveyBuilder AddHttpServiceClients(this IConveyBuilder builder)
        {
            builder.AddHttpClient("hds");
            // builder.Services.AddTransient<ICustomerServiceClient, CustomerServiceClient>();
            return builder;
        }
    }
}
