﻿// using Convey.HTTP;
// using HDS.PPC.ResourceService.Application.DTO.External;
// using HDS.PPC.ResourceService.Application.Services;
// using Microsoft.Extensions.Logging;
// using System.Threading.Tasks;

// namespace HDS.PPC.ResourceService.Infrastructure.Services.Clients
// {
//     public class CustomerServiceClient : ICustomerServiceClient
//     {
//         private readonly ILogger<CustomerServiceClient> _logger;
//         private readonly IHttpClient _httpClient;
//         private readonly string _url;

//         public CustomerServiceClient(ILogger<CustomerServiceClient> logger, IHttpClient httpClient, HttpClientOptions options)
//         {
//             _logger = logger;
//             _httpClient = httpClient;
//             _url = options.Services["customer"];
//         }

//         public async Task<CustomerInfoDto> GetClientInfo(string serviceAccountNumber)
//         {
//             _logger.LogInformation("Fetching ClientInfo for customer with {@serviceAccountNumber}", serviceAccountNumber);
//             var result = await _httpClient.GetAsync<CustomerInfoDto>($"{_url}/customer/{serviceAccountNumber}");
//             _logger.LogInformation("ClientInfo for {@serviceAccountNumber} received {@data}", serviceAccountNumber, result);
//             return result;
//         }
//     }
// }
