using Convey.CQRS.Events;
using HDS.PPC.ResourceService.Application.Services;
using HDS.PPC.ResourceService.Domain.Events;
using System.Collections.Generic;
using System.Linq;

namespace HDS.PPC.ResourceService.Infrastructure.Services
{
    public class EventMapper : IEventMapper
    {
        public IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events)
            => events.Select(Map);

        public IEvent Map(IDomainEvent @event)
        {
            // switch (@event)
            // {
            //     case ResourceStateChanged e:
            //         switch (e.Resource.Status)
            //         {
            //             case ResourceStatus.InProgress:
            //                 return new ResourceStarted(e.Resource.Id, e.Resource.StartedDateTime);
            //         }
            //         break;
            // }

            return null;
        }
    }
}