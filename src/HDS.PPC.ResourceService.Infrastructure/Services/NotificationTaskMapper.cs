﻿using HDS.PPC.ResourceService.Application.DTO;
using HDS.PPC.ResourceService.Application.Services;
using HDS.PPC.ResourceService.Domain.Aggregates;
using System.Collections.Generic;
using System.Linq;

namespace HDS.PPC.ResourceService.Infrastructure.Services
{
    public class ResourceMapper : IAggregateMapper<Resource, ResourceDto>
    {
        public ResourceDto Map(Resource source) => source.AsDto();

        public IEnumerable<ResourceDto> MapAll(IEnumerable<Resource> source) => source.Select(Map);
    }

    public static class ResourceMapperExtensions
    {
        public static ResourceDto AsDto(this Resource resource)
        {
            //Perform custom mapping
            return new ResourceDto();
        }
    }
}
