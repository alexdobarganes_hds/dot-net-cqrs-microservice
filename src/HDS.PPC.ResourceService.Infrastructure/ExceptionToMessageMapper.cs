using HDS.PPC.ResourceService.Application.Commands;
using HDS.PPC.ResourceService.Application.Events.Rejected;
using HDS.PPC.ResourceService.Application.Exceptions;
using System;

namespace HDS.PPC.ResourceService.Infrastructure.Exceptions
{
    public class ExceptionToMessageMapper : IExceptionToMessageMapper
    {
        public object Map(Exception exception, object message)
            => exception switch
            {
                ResourceAlreadyCreatedException ex => message switch
                {
                    CreateResource command => new CreateResourceRejected(command.ResourceId, ex.Message, ex.Code),
                    _ => null,
                },
                _ => null
            };
    }
}