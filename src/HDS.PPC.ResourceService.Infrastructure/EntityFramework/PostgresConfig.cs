﻿using System;

namespace HDS.PPC.ResourceService.Infrastructure.EntityFramework
{
    public class PostgresConfig
    {
        public int Port { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }

        public PostgresConfig()
        {
            UserId = Environment.GetEnvironmentVariable("DB_USR");
            Password = Environment.GetEnvironmentVariable("DB_PSW");
        }

        internal string GetConnectionString()
        {
            return $@"Server={Server};
                    Port={Port};
                    Database={Database};
                    User ID={UserId};
                    Password={Password};
                    Enlist=true;";
        }
    }
}
