﻿using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Application.Services;
using HDS.PPC.ResourceService.Domain.Aggregates;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Infrastructure.EntityFramework
{
    public class AggregateDbContext<T> : DbContext, IUnitOfWork where T : AggregateRoot
    {
        private readonly IEventProcessor _eventProcessor;
        private readonly IEntityTypeConfiguration<T> _aggregateEntityConfiguration;

        //private ICacheValidator<Dto> _cacheValidator;

        public DbSet<T> Aggregates { get; set; }
        public IEventProcessor EventProcessor => _eventProcessor;

        public AggregateDbContext(DbContextOptions<AggregateDbContext<T>> options,
            IEventProcessor eventProcessor,
            IEntityTypeConfiguration<T> _aggregateEntityConfiguration
            //ICacheValidator<Dto> cacheValidator
            ) : base(options)
        {
            _eventProcessor = eventProcessor;
            this._aggregateEntityConfiguration = _aggregateEntityConfiguration;
            //_cacheValidator = cacheValidator;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(_aggregateEntityConfiguration);
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            // 1. We persist the changes first
            _ = await SaveChangesAsync(cancellationToken);

            // If no exceptions, the code should continue, otherwise it exits after exception.

            // Let's grab all the Aggregates that changed state
            var entities = ChangeTracker.Entries();

            foreach (var entityEntry in entities)
            {
                // If it is not an Aggregate, we move to the next EntityEntry
                if (!(entityEntry.Entity is IAggregateRoot)) continue;

                // Let's parse the EntityEntry to is base type AggregateRoot so that we can
                // extract its events and id
                var aggregate = (AggregateRoot)entityEntry.Entity;

                // Let's remove any copy of the Aggregate's DTO from Redis.
                // Since the state of the aggregate changed, the cache is invalid.
                // Wipe it out! :)

                // This needs refactoring... do not use for now.
                //await _cacheValidator.Invalidate(aggregate.Id.ToString());

                // Let's dispatch all the events that accumulated during the Aggregate state changes
                await _eventProcessor.ProcessAsync(aggregate.Events);
            }

            return true;
        }
    }
}
