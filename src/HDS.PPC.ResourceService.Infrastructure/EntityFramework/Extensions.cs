﻿using Convey;
using HDS.PPC.ResourceService.Application.DTO;
using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Application.Services;
using HDS.PPC.ResourceService.Domain.Aggregates;
using HDS.PPC.ResourceService.Infrastructure.EntityFramework.Configuration;
using HDS.PPC.ResourceService.Infrastructure.Postgres.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace HDS.PPC.ResourceService.Infrastructure.EntityFramework
{
    public static class Extensions
    {
        public static IConveyBuilder AddDbRepositoriesAndMappers<TAggregate, TDto, TMapper>(this IConveyBuilder builder)
            where TAggregate : AggregateRoot
            where TDto : Dto
            where TMapper : IAggregateMapper<TAggregate, TDto>
        {
            builder.Services.AddScoped<IRepository<TAggregate>, PostgresRepository<TAggregate>>();
            builder.Services.AddScoped<IReadRepository<TAggregate, TDto, Guid>, PostgresReadRepository<TAggregate, TDto>>();
            builder.Services.AddScoped(typeof(IAggregateMapper<TAggregate, TDto>), typeof(TMapper));

            return builder;
        }

        public static IConveyBuilder AddPostgresDb<TAggregate, TConfiguration>(this IConveyBuilder builder)
            where TAggregate : AggregateRoot
            where TConfiguration : BaseEntityTypeConfiguration<TAggregate>
        {
            builder.Services.AddSingleton(builder.GetOptions<PostgresConfig>("postgres"));
            builder.Services.AddSingleton<IEntityTypeConfiguration<TAggregate>, TConfiguration>();
            builder.Services.AddDbContext<AggregateDbContext<TAggregate>>((provider, optionsBuilder) =>
            {
                var config = provider.GetService<PostgresConfig>();
                optionsBuilder
                .UseNpgsql(config.GetConnectionString(), options =>
                {
                    options.EnableRetryOnFailure();
                    options.SetPostgresVersion(13, 3);
                });
            });

            return builder;
        }

        public static IApplicationBuilder UsePostgresDb<T>(this IApplicationBuilder app) where T : AggregateRoot
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()?.CreateScope())
            {
                var context = serviceScope?.ServiceProvider.GetRequiredService<AggregateDbContext<T>>();
                context?.Database.EnsureCreated();
            }

            return app;
        }
    }
}
