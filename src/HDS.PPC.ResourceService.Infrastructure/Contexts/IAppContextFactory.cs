using HDS.PPC.ResourceService.Application;

namespace HDS.PPC.ResourceService.Infrastructure.Contexts
{
    internal interface IAppContextFactory
    {
        IAppContext Create();
    }
}