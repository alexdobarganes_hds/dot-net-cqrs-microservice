using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Application.Services;
using HDS.PPC.ResourceService.Domain.Aggregates;
using HDS.PPC.ResourceService.Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Infrastructure.Postgres.Repositories
{
    public class PostgresReadRepository<TAggregate, TDto> : IReadRepository<TAggregate, TDto, Guid>
        where TAggregate : AggregateRoot
        where TDto : class
    {
        private readonly AggregateDbContext<TAggregate> _dbContext;
        private readonly IAggregateMapper<TAggregate, TDto> _aggregateMapper;

        public PostgresReadRepository(AggregateDbContext<TAggregate> dbContext, IAggregateMapper<TAggregate, TDto> aggregateMapper)
        {
            _dbContext = dbContext;
            _dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            _aggregateMapper = aggregateMapper;
        }

        public async Task<TDto> GetAsync(Guid id)
        {
            var aggregate = await _dbContext.FindAsync<TAggregate>(id);

            if (aggregate == null)
                return null;

            return _aggregateMapper.Map(aggregate);
        }

        public Task<IEnumerable<TDto>> FindAsync(Expression<Func<TAggregate, bool>> findPredicate)
        {
            var aggregates = _dbContext.Aggregates.Where(findPredicate).ToList();
            return Task.FromResult(_aggregateMapper.MapAll(aggregates));
        }
    }
}