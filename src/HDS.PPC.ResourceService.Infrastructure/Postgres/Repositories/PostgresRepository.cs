﻿using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Domain.Aggregates;
using HDS.PPC.ResourceService.Infrastructure.EntityFramework;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Infrastructure.Postgres.Repositories
{
    public class PostgresRepository<TAggregate> : IRepository<TAggregate> where TAggregate : AggregateRoot
    {
        private readonly AggregateDbContext<TAggregate> _dbContext;

        public PostgresRepository(AggregateDbContext<TAggregate> dbContext)
            => _dbContext = dbContext;

        public IUnitOfWork UnitOfWork => _dbContext;

        public async Task AddAsync(TAggregate aggregate) => await _dbContext.AddAsync(aggregate);

        public async Task<TAggregate> GetAsync(Guid id) => await _dbContext.FindAsync<TAggregate>(id);

        public Task UpdateAsync(TAggregate aggregate)
        {
            _dbContext.Update(aggregate);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(TAggregate aggregate)
        {
            _ = _dbContext.Remove(aggregate);
            return Task.CompletedTask;
        }
    }
}
