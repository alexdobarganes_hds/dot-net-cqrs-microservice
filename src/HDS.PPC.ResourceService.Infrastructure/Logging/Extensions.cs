using Convey;
using Convey.Logging.CQRS;
using HDS.PPC.ResourceService.Application.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace HDS.PPC.ResourceService.Infrastructure.Logging
{
    internal static class Extensions
    {
        public static IConveyBuilder AddHandlersLogging(this IConveyBuilder builder)
        {
            var assembly = typeof(CreateResource).Assembly;

            builder.Services.AddSingleton<IMessageToLogTemplateMapper>(new MessageToLogTemplateMapper());

            return builder
                .AddCommandHandlersLogging(assembly)
                .AddEventHandlersLogging(assembly);
        }
    }
}