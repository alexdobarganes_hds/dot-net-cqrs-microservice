using Convey.Logging.CQRS;
using HDS.PPC.ResourceService.Application.Commands;
using HDS.PPC.ResourceService.Application.Exceptions;
using System;
using System.Collections.Generic;

namespace HDS.PPC.ResourceService.Infrastructure.Logging
{
    internal sealed class MessageToLogTemplateMapper : IMessageToLogTemplateMapper
    {
        private static IReadOnlyDictionary<Type, HandlerLogTemplate> MessageTemplates
            => new Dictionary<Type, HandlerLogTemplate>
            {
                 {
                     typeof(CreateResource),
                     new HandlerLogTemplate
                     {
                         After = "Created a Resource with id: {ResourceId}. ",
                         OnError = new Dictionary<Type, string>
                         {
                             { typeof(ResourceAlreadyCreatedException), "Resource with id: {ResourceId} already created."}
                         }
                     }
                 }
            };

        public HandlerLogTemplate Map<TMessage>(TMessage message) where TMessage : class
        {
            var key = message.GetType();
            return MessageTemplates.TryGetValue(key, out var template) ? template : null;
        }
    }
}