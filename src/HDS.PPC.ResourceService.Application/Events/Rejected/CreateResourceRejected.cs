﻿using Convey.CQRS.Events;
using System;

namespace HDS.PPC.ResourceService.Application.Events.Rejected
{
    public class CreateResourceRejected : IEvent
    {
        public Guid ResourceId { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }

        public CreateResourceRejected(Guid resourceId, string message, string code)
        {
            ResourceId = ResourceId;
            Message = message;
            Code = code;
        }
    }
}