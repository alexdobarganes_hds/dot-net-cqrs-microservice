﻿// using Convey.CQRS.Commands;
// using Convey.CQRS.Events;
// using Convey.MessageBrokers;
// using HDS.PPC.ResourceService.Application.Commands;
// using HDS.PPC.ResourceService.Application.Commands.External;
// using HDS.PPC.ResourceService.Application.Repositories;
// using HDS.PPC.ResourceService.Domain.Aggregates;
// using System;
// using System.Linq;
// using System.Threading.Tasks;

// namespace HDS.PPC.ResourceService.Application.Events.Handlers
// {
//     public class ResourceCreatedHandler : IEventHandler<ResourceCreated>
//     {
//         private readonly IBusPublisher _busPublisher;
//         private readonly ICommandDispatcher _commandDispatcher;
//         private readonly IRepository<Resource> _repository;

//         public ResourceCreatedHandler(IBusPublisher busPublisher, ICommandDispatcher commandDispatcher, IRepository<Resource> repository)
//         {
//             _busPublisher = busPublisher;
//             _commandDispatcher = commandDispatcher;
//             _repository = repository;
//         }
//         public async Task HandleAsync(ResourceCreated @event)
//         {
//             var task = await _repository.GetAsync(@event.ResourceId);
//             if (task.Content != null)
//             {
//                 var updateCardsHistory = new UpdateCardsHistory(@event.ResourceId, task.HistoryRecords.Select(x => x.AsDto()).ToList());
//                 await _commandDispatcher.SendAsync(updateCardsHistory);
//             }
//             else
//             {
//                 var command = new CompleteResource(task.Id, DateTimeOffset.Now);
//                 await _commandDispatcher.SendAsync(command);
//             }
//         }
//     }
// }
