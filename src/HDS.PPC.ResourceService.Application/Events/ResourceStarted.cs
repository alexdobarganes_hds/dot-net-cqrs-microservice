using Convey.CQRS.Events;
using Convey.WebApi.CQRS;
using System;

namespace HDS.PPC.ResourceService.Application.Events
{
    [PublicContract]
    public class ResourceCreated : IEvent
    {
        public Guid ResourceId { get; }

        public DateTimeOffset StartedDateTime { get; }

        public ResourceCreated(Guid ResourceId, DateTimeOffset startedDateTime)
        {
            ResourceId = ResourceId;
            StartedDateTime = startedDateTime;
        }
    }
}