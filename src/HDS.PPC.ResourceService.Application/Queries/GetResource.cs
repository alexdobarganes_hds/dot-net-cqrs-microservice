﻿using Convey.CQRS.Queries;
using Convey.WebApi.CQRS;
using HDS.PPC.ResourceService.Application.DTO;
using System;

namespace HDS.PPC.ResourceService.Application.Queries
{
    [PublicContract]
    public class GetResource : IQuery<ResourceDto>
    {
        public Guid ResourceId { get; set; }
    }
}
