﻿using Convey.CQRS.Queries;
using HDS.PPC.ResourceService.Application.DTO;
using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Application.Queries.Handlers
{
    public class GetResourceHandler : IQueryHandler<GetResource, ResourceDto>
    {
        private readonly IReadRepository<Resource, ResourceDto, Guid> _readRepository;

        public GetResourceHandler(IReadRepository<Resource, ResourceDto, Guid> readRepository)
        {
            _readRepository = readRepository;
        }

        public async Task<ResourceDto> HandleAsync(GetResource query)
        {
            var Resource = await _readRepository.GetAsync(query.ResourceId);

            return Resource;
        }
    }
}
