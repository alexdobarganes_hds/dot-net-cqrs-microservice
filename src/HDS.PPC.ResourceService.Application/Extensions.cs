using Convey;
using Convey.CQRS.Commands;
using Convey.CQRS.Events;
using Convey.CQRS.Queries;

namespace HDS.PPC.ResourceService.Application
{
    public static class Extensions
    {
        public static IConveyBuilder AddApplication(this IConveyBuilder builder)
            => builder
                .AddQueryHandlers()
                .AddCommandHandlers()
                .AddEventHandlers()
                .AddInMemoryQueryDispatcher()
                .AddInMemoryCommandDispatcher()
                .AddInMemoryEventDispatcher();
    }
}