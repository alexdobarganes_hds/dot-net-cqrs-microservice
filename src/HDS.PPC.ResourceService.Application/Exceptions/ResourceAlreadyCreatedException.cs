using System;

namespace HDS.PPC.ResourceService.Application.Exceptions
{
    public class ResourceAlreadyCreatedException : AppException
    {
        public override string Code { get; } = "resource_already_created";

        public ResourceAlreadyCreatedException(Guid resourceId) : base($"Resource with id: {resourceId} was already created.")
        {
        }
    }
}