﻿using System;

namespace HDS.PPC.ResourceService.Application.DTO
{
    public abstract class Dto
    {
        public Guid Id { get; set; }
    }
}
