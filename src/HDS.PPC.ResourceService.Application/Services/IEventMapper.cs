using Convey.CQRS.Events;
using HDS.PPC.ResourceService.Domain.Events;
using System.Collections.Generic;

namespace HDS.PPC.ResourceService.Application.Services
{
    public interface IEventMapper
    {
        IEvent Map(IDomainEvent @event);
        IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events);
    }
}