﻿using HDS.PPC.ResourceService.Domain.Events;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Application.Services
{
    public interface IEventProcessor
    {
        Task ProcessAsync(IEnumerable<IDomainEvent> events);
    }
}