using Convey.CQRS.Commands;
using Convey.WebApi.CQRS;
using System;

namespace HDS.PPC.ResourceService.Application.Commands
{

    [PublicContract]
    public class CreateResource : ICommand
    {
        public Guid ResourceId { get; set; }
        public string SomeArgument { get; set; }

        public CreateResource(string someArgument)
        {
            ResourceId = Guid.NewGuid();
            SomeArgument = someArgument;
        }
    }
}