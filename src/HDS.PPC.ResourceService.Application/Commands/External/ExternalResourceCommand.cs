﻿using Convey.CQRS.Commands;

using HDS.PPC.ResourceService.Application.Events;
using System;

namespace HDS.PPC.ResourceService.Application.Commands.External
{
    [PubSubInfo("topic", "")]
    public class ExternalResourceCommand : ICommand
    {
        public Guid BusinessTaskId { get; }
        public string Argument { get; }

        public ExternalResourceCommand(Guid businessTaskId, string argument)
        {
            BusinessTaskId = businessTaskId;
            Argument = argument;
        }
    }
}
