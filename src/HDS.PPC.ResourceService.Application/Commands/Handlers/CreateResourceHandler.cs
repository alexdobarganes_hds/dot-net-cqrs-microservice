﻿using Convey.CQRS.Commands;
using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Domain.Aggregates;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Application.Commands.Handlers
{
    public class CreateResourceHandler : ICommandHandler<CreateResource>
    {
        private readonly IRepository<Resource> _repository;

        public CreateResourceHandler(IRepository<Resource> repository)
        {
            _repository = repository;
        }
        public async Task HandleAsync(CreateResource command)
        {
            var resource = Resource.Create(command.ResourceId, command.SomeArgument);
            await _repository.AddAsync(resource);
            await _repository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}