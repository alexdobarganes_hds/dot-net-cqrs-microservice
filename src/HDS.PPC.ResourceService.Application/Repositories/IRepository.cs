﻿using HDS.PPC.ResourceService.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Application.Repositories
{
    public interface IRepository<TAggregate>
         where TAggregate : AggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
        Task<TAggregate> GetAsync(Guid id);
        Task AddAsync(TAggregate aggregate);
        Task UpdateAsync(TAggregate aggregate);
        Task DeleteAsync(TAggregate aggregate);
    }
}
