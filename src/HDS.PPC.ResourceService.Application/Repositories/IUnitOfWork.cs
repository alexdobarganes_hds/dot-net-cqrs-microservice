﻿using HDS.PPC.ResourceService.Application.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Application.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IEventProcessor EventProcessor { get; }
        Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default);
    }
}
