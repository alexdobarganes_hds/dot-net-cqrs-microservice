using Convey;
using Convey.Logging;
using Convey.Types;
using Convey.WebApi;
using Convey.WebApi.CQRS;
using HDS.PPC.ResourceService.Application;
using HDS.PPC.ResourceService.Application.Commands;
using HDS.PPC.ResourceService.Application.DTO;
using HDS.PPC.ResourceService.Application.Queries;
using HDS.PPC.ResourceService.Infrastructure;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Formatting.Json;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService
{
    public class Program
    {
        public static async Task Main(string[] args)
            => await WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(services => services
                    .AddConvey()
                    .AddWebApi()
                    .AddApplication()
                    .AddInfrastructure()
                    .Build())
                .Configure(app => app
                    .UseApp()
                    .UseRouting()
                    .UsePublicContracts()
                    .UseEndpoints(endpoints => endpoints.MapHealthChecks("/health"))
                    .UseDispatcherEndpoints(endpoints => endpoints
                        .Get("", ctx => ctx.Response.Ok(app.ApplicationServices.GetService<AppOptions>()))
                        .Get<GetResource, ResourceDto>("resources/{ResourceId}")
                        .Post<CreateResource>("resources",
                            afterDispatch: (cmd, ctx) => ctx.Response.Created($"resources/{cmd.ResourceId}"))))
                .UseLogging((c, config) => config.WriteTo.Console(new JsonFormatter()))
                .Build()
                .RunAsync();
    }
}