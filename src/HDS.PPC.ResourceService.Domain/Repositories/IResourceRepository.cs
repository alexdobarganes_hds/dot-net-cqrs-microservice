using HDS.PPC.ResourceService.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Domain.Repositories
{
    public interface IResourceRepository
    {
        Task<Resource> GetAsync(Guid id);
        Task AddAsync(Resource Resource);
        Task UpdateAsync(Resource Resource);
        Task DeleteAsync(Resource id);
    }
}