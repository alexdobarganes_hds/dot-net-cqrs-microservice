using HDS.PPC.ResourceService.Domain.Events;
using System.Collections.Generic;

namespace HDS.PPC.ResourceService.Domain.Aggregates
{
    public interface IAggregateRoot
    {
        IEnumerable<IDomainEvent> Events { get; }
    }
}
