using HDS.PPC.ResourceService.Domain.Events;
using System;

namespace HDS.PPC.ResourceService.Domain.Aggregates
{
    public class Resource : AggregateRoot
    {
        public string Content { get; protected set; }
        protected Resource() { }

        private Resource(Guid id, string content)
        {
            Id = id;
            Content = content;

            AddEvent(new ResourceStateChanged(this));
        }

        public static Resource Create(Guid id, string content)
        {
            var Resource = new Resource(id, content);

            return Resource;
        }
    }
}