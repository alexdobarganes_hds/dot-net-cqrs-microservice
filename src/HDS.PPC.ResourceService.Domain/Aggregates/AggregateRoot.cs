﻿using HDS.PPC.ResourceService.Domain.Events;
using System;
using System.Collections.Generic;

namespace HDS.PPC.ResourceService.Domain.Aggregates
{
    public abstract class AggregateRoot : Entity<Guid>, IAggregateRoot
    {
        private readonly List<IDomainEvent> _events = new List<IDomainEvent>();
        public IEnumerable<IDomainEvent> Events => _events;

        protected void AddEvent(IDomainEvent @event)
        {
            _events.Add(@event);
        }

        public void ClearEvents() => _events.Clear();
    }
}
