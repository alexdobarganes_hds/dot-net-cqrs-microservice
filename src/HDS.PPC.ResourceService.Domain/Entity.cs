﻿namespace HDS.PPC.ResourceService.Domain
{
    public abstract class Entity<TId>
    {
        public TId Id { get; set; }
    }
}
