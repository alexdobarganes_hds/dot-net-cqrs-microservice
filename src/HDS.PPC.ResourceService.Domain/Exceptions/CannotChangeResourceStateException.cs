using System;

namespace HDS.PPC.ResourceService.Domain.Exceptions
{
    public class CannotChangeResourceStateException : DomainException
    {
        public override string Code { get; } = "cannot_change_resource_state";

        public CannotChangeResourceStateException(Guid id) :
            base($"Cannot change state for Resource with id: '{id}'")
        { }
    }
}