﻿using System.Threading.Tasks;

namespace HDS.PPC.ResourceService.Domain.Events
{
    public interface IDomainEventHandler<in T> where T : class, IDomainEvent
    {
        Task HandleAsync(T @event);
    }
}
