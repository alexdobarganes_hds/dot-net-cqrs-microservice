﻿using HDS.PPC.ResourceService.Domain.Aggregates;

namespace HDS.PPC.ResourceService.Domain.Events
{
    public record ResourceStateChanged : IDomainEvent
    {
        public Resource Resource { get; }

        public ResourceStateChanged(Resource Resource)
        {
            Resource = Resource;
        }
    }
}
