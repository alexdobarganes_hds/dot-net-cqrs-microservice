using AutoFixture;
using HDS.PPC.ResourceService.Domain.Aggregates;
using System;
using Xunit;

namespace HDS.PPC.ResourceService.Domain.Tests.Aggregates
{
    public class ResourceTests
    {
        private readonly Fixture _fixture;
        private Resource _sut;

        public ResourceTests()
        {
            _fixture = new Fixture();

            
        }

        [Fact]
        public void ShouldBeAbleToBeCompleted()
        {
            var id = _fixture.Create<Guid>();
            var content = _fixture.Create<string>();

            _sut = Resource.Create(id, content);

            Assert.Equal(id, _sut.Id);
            Assert.Equal(content, _sut.Content);
            Assert.NotEmpty(_sut.Events);
        }
    }
}