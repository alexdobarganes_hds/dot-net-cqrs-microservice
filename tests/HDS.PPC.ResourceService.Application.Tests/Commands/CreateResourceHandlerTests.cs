using AutoFixture;
using Convey.CQRS.Commands;
using HDS.PPC.ResourceService.Application.Commands;
using HDS.PPC.ResourceService.Application.Commands.Handlers;
using HDS.PPC.ResourceService.Application.Repositories;
using HDS.PPC.ResourceService.Domain.Aggregates;
using Moq;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace HDS.PPC.ResourceService.Application.Tests.Commands
{

    public class CreateResourceHandlerTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<IRepository<Resource>> _repositoryMock;
        private readonly ICommandHandler<CreateResource> _sut;

        private Task Act(CreateResource command) => _sut.HandleAsync(command);

        public CreateResourceHandlerTests()
        {
            _fixture = new Fixture();
            _repositoryMock = new Mock<IRepository<Resource>>();
            _repositoryMock.Setup(r => r.UnitOfWork.SaveEntitiesAsync(It.IsAny<CancellationToken>()));
            _sut = new CreateResourceHandler(_repositoryMock.Object);
        }

        [Fact]
        public async Task ShouldCreateNewResource() 
        {
            //Given
            var command = new CreateResource(_fixture.Create<string>());
            Resource resource = null;
            _repositoryMock.Setup(r => r.AddAsync(It.IsAny<Resource>())).Callback<Resource>(res => {
                resource = res;
            });

            //When
            await Act(command);

            //Then
            _repositoryMock.Verify(x => x.AddAsync(It.IsAny<Resource>()), Times.AtLeastOnce);
            _repositoryMock.Verify(x => x.UnitOfWork.SaveEntitiesAsync(default), Times.Once);
            Assert.Equal(command.ResourceId, resource.Id);
            Assert.Equal(command.SomeArgument, resource.Content);
        }
    }
}